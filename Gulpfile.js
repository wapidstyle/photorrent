//
// Gulpfile for Photorrent
//
const gulp = require("gulp");

gulp.task("watch-less", () => {
  return require("gulp-watch")("src/less/*.less")
    .pipe(require("gulp-less")())
    .pipe(gulp.dest("output/css"));
});

gulp.task("watch-babel", () => {
  return require("gulp-watch")("src/**/*.js")
    .pipe(require("gulp-babel")({
      presets: ["latest"]
    }))
    .pipe(gulp.dest("output/"));
});

gulp.task("less", () => {
  return gulp.src("src/less/*.less")
    .pipe(require("gulp-less")())
    .pipe(gulp.dest("output/css"));
});

gulp.task("babel", () => {
  return gulp.src("src/**/*.js")
    .pipe(require("gulp-babel")({
      presets: ["latest"]
    }))
    .pipe(gulp.dest("output/"));
});

gulp.task("run", ["babel", "less"], () => {
  require("child_process").fork(__dirname + "/output/index.js");
});
