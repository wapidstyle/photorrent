//
// Controller indexor for Photorrent.
//
// This code is under the BSD-3 license.
//
import {default as dbs} from "./standarddb";

import {default as wiki} from "./controllers/wiki";
import {default as css} from "./controllers/css";

/**
  * Main function for the indexor.
  *
  * @returns {Null} nothing
  */
export default function indexor() {
  let app = dbs.get("app");

  app.get("/*/*/wiki", wiki);
  app.get("/css/*", css);
}
