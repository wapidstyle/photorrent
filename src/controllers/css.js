//
// CSS controller for Photorrent.
//
// This code is under the BSD-3 license.
//
import {default as fp} from "../fileprocessor";

export default function css(req, res) {
  fp(res, "../../output/css/" + req.url.split("/")[2], {}, req);
}
