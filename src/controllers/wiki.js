//
// Main controller for Photorrent. Gets a wiki page.
//
// This code is under the MIT license.
//
import {default as fp} from "../fileprocessor";
import {default as mongoose} from "mongoose";

export default function wiki(req, res) {
  fp(res, "wiki.html", {}, req);
}
