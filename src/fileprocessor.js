//
// File processor for Photorrent, meant to process files with all
// necessary variables.
//
// This code is under the BSD-3 license.
//
import {default as handpipe} from "handpipe";
import {default as fs} from "graceful-fs";
import {default as dbs} from "./standarddb";
import {default as _} from "lodash";
import {default as winston} from "winston";

winston.loggers.add("fp", {
  console: {
    level: "warn",
    colorize: true,
    label: "fp"
  }
});

let logger = winston.loggers.get("fp");
let directory = __dirname + dbs.get("config");


/**
  * Processes input files twice, to provide navbars, etc. Includes all variables.
  *
  * @param {Stream} res The response stream to pipe to
  * @param {String} file The path of the file
  * @param {Object} custom Custom parameters to pass along with default ones.
  * @param {Stream} req The request passed to the controller
  * @returns {Null} Nothing
  */
export default function fileProcessor(res, file, custom, req) {
  fs.access(__dirname + "/../src/static/" + file, (err) => {
    try {
      if(err) throw err;

      fs.createReadStream(directory + file)
        .pipe(handpipe(_.extend(dbs.get("vars"), custom, {
          genericPage: fs.readFileSync(directory + "generic.html"),
          repoHeader: fs.readFileSync(directory + "repoheader.html"),
          footer: fs.readFileSync(directory + "footer.html")
        })))
        .pipe(handpipe(_.extend(dbs.get("vars"), dbs.get("config"), {
          user: req.url.split("/")[1],
          repo: req.url.split("/")[2],
          // HACK until we get valid public keys/ids, use this REAL one. (wink)
          hash: "9pegnprr98g4h98govnnrhtng8vuirunvgj8orgvjo948vnyth8u5nhgouvt4jgv"
        })))
        .pipe(res);
    } catch(e) {
      logger.warn("Request for " + file + " returned with error: " + e.message.split(",")[0]);
      if(e.code === "ENOENT") {
        res.status(404).end();
      } else {
        res.status(500).end();
      }
    }
  });
}
