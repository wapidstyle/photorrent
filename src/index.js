//
// Photorrent: an online mirror for GitTorrent.
//
// This code is under the BSD-3 license.
//
import {default as express} from "express";
import {default as winston} from "winston";
import {default as mongoose} from "mongoose";
import {default as yaml} from "js-yaml";
import {default as fs} from "graceful-fs";
import {default as dbs} from "./standarddb";
import {default as schemas} from "./schemas";
import {default as controllerindexor} from "./controllerindexor";
import {default as _} from "lodash";

winston.loggers.add("main", {
  console: {
    level: "info",
    colorize: true,
    label: "main"
  }
});

/**
  * Main function for Photorrent. Runs the Express server.
  * @return {null} nothing Nothing.
  */
process.nextTick(function photorrent() {
  "use strict";

  const logger = winston.loggers.get("main");

  logger.info("Starting photorrent...");

  logger.info("Reading config...");
  // TODO Use Promises
  fs.readFile(__dirname + "/../config.yml", "utf-8", (err, data) => {
    if(err) {
      logger.error("Failed to read config file: " + err.message);
      throw err;
    }

    dbs.set("config", yaml.load(data));

    logger.info("Connecting to database...");
    let config = dbs.get("config");
    mongoose.connect("mongodb://" + config.db.user + ":" + process.env[config.db.pass] +
                      "@" + config.db.url + ":" + config.db.port + "/" + config.db.database);

    mongoose.connection.on("error", (err) => {
      logger.error("Something bad happened while connecting to MongoDB!");
      logger.error("Attempted to connect to: " + "mongodb://" + config.db.user
                      + ":" + process.env[config.db.pass]
                      + "@" + config.db.url + ":" + config.db.port + "/"
                      + config.db.database);
      throw err;
    });

    mongoose.connection.on("open", () => {
      logger.info("Connected to DB, setting schemas...");
      dbs.set("schemas", schemas);
      logger.info("Starting server...");

      dbs.set("vars", _.extend(config, {}));

      dbs.set("app", express());

      let app = dbs.get("app");
      controllerindexor();
      dbs.get("app").listen(config.port);
    });
  });
});
