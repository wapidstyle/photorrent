//
// Schema setter for Photorrent.
//
// This code is under the MIT license.
//
let schemas = {
  wiki: {
    repoId: String,
    wikiHeader: String,
    wikiContents: Array,
    wikiFooter: String
  }
};

export default schemas;
