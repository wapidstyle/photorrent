//
// Storage for Photorrent, meant for storing LokiDB's.
//
// This code is under the BSD-3 License.
//
let dbs = {
  set: function setDbEntry(name, value) {
    dbs[name] = value;
  },
  get: function getDbEntry(name) {
    return dbs[name];
  }
};

export default dbs;
